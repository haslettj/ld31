package me.haslett.ld31;

public abstract class Action {

	protected boolean fireOnce = true;
	protected int fireCount;

	public Action( boolean fireOnce ) {
		this.fireOnce = fireOnce;
	}

	public abstract int go( Level level );

	public int getFireCount() {
		return fireCount;
	}

}
