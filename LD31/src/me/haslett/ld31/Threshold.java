package me.haslett.ld31;

import me.haslett.collections.AVLTree;
import me.haslett.collections.MapEntry;

import org.newdawn.slick.Color;

public class Threshold extends Entity {

	
	protected AVLTree<Integer, Action> _actions;

	protected int nextEntityKey = 0;


	public Threshold( float x, float y, float width, float height, boolean canMove ) {
		super( x, y, width, height, canMove, false, true );
		
		_actions = new AVLTree<Integer, Action>();

		hitBoxColor = Color.cyan;
		
		showHitBox = false;

	}


	public int addAction( Action a ) throws Exception {
		int key = nextEntityKey;
		_actions.Add( key, a );

		nextEntityKey++;

		return key;
	}

	public void removeAction( int key ) {
		_actions.Remove( key );

	}
	
	public void triggerActions( Level level){
		for ( MapEntry<Integer, Action> map : _actions ){
			map.getValue().go( level );
		}
	}


	public void addActions( Action[] actions ) throws Exception {
		for(Action a : actions){
			addAction( a );
		}
	}
	


}
