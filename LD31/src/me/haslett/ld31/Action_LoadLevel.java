package me.haslett.ld31;

public class Action_LoadLevel extends Action {

	protected int levelNumber;
	public Action_LoadLevel( int levelNumber, boolean fireOnce ) {
		super( fireOnce );
		
		this.levelNumber = levelNumber;
	}

	@Override
	public int go( Level level ) {
		try {
			level.loadLevel( levelNumber );
		} catch ( Exception e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
