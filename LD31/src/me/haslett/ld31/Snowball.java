package me.haslett.ld31;

import java.util.ArrayList;

import me.haslett.collections.MapEntry;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Snowball extends Entity {

	public static final String SNOWBALL_SPRITES_FILE_NAME = "res/snowball.png";
	public static final String NAME = "Snowball";
	public static final float MOVE_ACCELERATION = 1.0f;
	public static final float MOVE_MAX_SPEED = 4.0f;

	public static final float WIDTH = 8f;
	public static final float HEIGHT = 8f;

	public Snowball( float x, float y ) throws SlickException {
		super( x, y, WIDTH, HEIGHT, true, false, false );

		_name = NAME;

		moveAccel = MOVE_ACCELERATION;
		moveMaxSpeed = MOVE_MAX_SPEED;

		animationSheets = new SpriteSheet[1];
		animationSheets[0] = new SpriteSheet( SNOWBALL_SPRITES_FILE_NAME, (int) WIDTH, (int) HEIGHT );

		animations = new Animation[1];
		animations[0] = new Animation( animationSheets[0], 0, 0, 3, 0, true, 200, true );
		animations[0].setPingPong( false );
		animations[0].setLooping( true );

		currentAnimation = 0;
	}

	@Override
	public void update( Level level ) {

		if ( canMove ) {

			if ( velocityX != 0 ) {
				float xOffset = hitBox.getMaxX();
				float yOffset = hitBox.getMaxY();

				int direction;

				x += velocityX;

				if ( velocityX > 0 ) {
					direction = MOVE_DIRECTION_RIGHT;
				} else {
					direction = MOVE_DIRECTION_LEFT;
				}

				ArrayList<MapEntry<Integer, Entity>> collisions = level.CollidesWith( this );
				for ( MapEntry<Integer, Entity> map : collisions ) {
					if ( map.getValue() != this ) {
						float collisionMinX = map.getValue().getWorldCollisionBox().getX();
						float collisionMinY = map.getValue().getWorldCollisionBox().getY();
						float collisionMaxY = map.getValue().getWorldCollisionBox().getMaxY();

						if ( ( collisionMinY > y && collisionMinY < y + yOffset ) || ( collisionMaxY > y && collisionMaxY < y + yOffset ) || ( y > collisionMinY && y < collisionMaxY )
								|| ( y + yOffset > collisionMinY && y + yOffset < collisionMaxY ) ) {

							if ( map.getValue().getBlocksOnCollision() ) {
								if ( direction == MOVE_DIRECTION_RIGHT ) {
									x = collisionMinX - xOffset - 1;
									velocityX = 0;

								} else if ( direction == MOVE_DIRECTION_LEFT ) {
									x = map.getValue().getWorldCollisionBox().getMaxX() + 1 - hitBox.getX();
									velocityX = 0;

								}
								level.snowballHit(this, map);
							}

						}
					}
				}

			}

		}
	}

	public void setVelocityX( float f ) {
		velocityX = f;
	}



}
