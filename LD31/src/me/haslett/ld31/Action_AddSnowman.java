package me.haslett.ld31;

public class Action_AddSnowman extends Action {

	protected int mode;
	protected float x, y;

	protected Snowman snowman;

	public Action_AddSnowman( int mode, float x, float y, boolean fireOnce ) {
		super( fireOnce );

		this.mode = mode;
		this.x = x;
		this.y = y;
	}

	@Override
	public int go( Level level ) {

		try {
			level.addEntity( new Snowman( x, y, mode ) );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return 0;
	}
}
