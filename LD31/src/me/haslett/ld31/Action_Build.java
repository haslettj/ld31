package me.haslett.ld31;

import me.haslett.collections.AVLTree;
import me.haslett.collections.MapEntry;

public class Action_Build extends Action {

	protected AVLTree<Integer, Entity> _entities;

	protected int nextEntityKey = 0;

	public Action_Build( boolean fireOnce ) {
		super( fireOnce );
		_entities = new AVLTree<Integer, Entity>();

	}

	public int addEntity( Entity e ) throws Exception {
		int key = nextEntityKey;
		_entities.Add( key, e );

		nextEntityKey++;

		return key;
	}

	public void removeEntity( int key ) {
		_entities.Remove( key );

	}

	@Override
	public int go( Level level ) {
		if ( !fireOnce || fireCount < 1 ) {
			fireCount++;
			
			for ( MapEntry<Integer, Entity> map : _entities ) {
				try {
					level.addEntity( map.getValue() );
				} catch ( Exception e ) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return 0;
	}

}
