package me.haslett.ld31;

public class Action_AtHome extends Action {

	public Action_AtHome( boolean fireOnce ) {
		super( fireOnce );
	}

	@Override
	public int go( Level level ) {
		level.setGameOver( true );
		return 0;
	}

}
