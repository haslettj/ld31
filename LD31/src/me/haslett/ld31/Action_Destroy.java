package me.haslett.ld31;

import java.util.ArrayList;

public class Action_Destroy extends Action {

	protected ArrayList<Integer> _indexes;

	public Action_Destroy( boolean fireOnce ) {
		super( fireOnce );
		_indexes = new ArrayList<Integer>();

	}

	public void addIndex( int index ) {
		_indexes.add( index );
	}

	public void removeIndex( int index ) {
		_indexes.remove( index );
	}

	@Override
	public int go( Level level ) {

		if ( !fireOnce || fireCount < 1 ) {
			fireCount++;
			for ( Integer i : _indexes ) {
				level.removeEntity( i );
			}
		}
		return 0;

	}
}
