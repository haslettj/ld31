package me.haslett.ld31;

public class Action_SetGameState extends Action{

	protected boolean giveParachute;
	
	public Action_SetGameState( boolean giveParachute, boolean fireOnce ) {
		super( fireOnce );
		
		this.giveParachute = giveParachute;

	}

	@Override
	public int go( Level level ) {
		if(giveParachute)level.getPlayer().setParachute(true);
		return 0;
	}

}
