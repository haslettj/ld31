package me.haslett.ld31;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.ResourceLoader;

import me.haslett.collections.AVLTree;
import me.haslett.collections.MapEntry;

public class Level {
	public static final float INITIAL_GRAVITY = 0.4f;
	public static final float INITIAL_FRICTION = 0.5f;

	public static final float STARTING_X = 10f;
	public static final float STARTING_Y = 485f;

	protected float gravity;
	protected float friction;

	protected Resources resources;

	// protected Snowman snowman;

	protected Snowball snowball;
	protected int snowballKey;

	protected Player player;

	protected AVLTree<Integer, Entity> _entities;

	protected int nextEntityKey = 0;

	protected boolean hasPlayedFirstSnowballHit = false;

	protected int currentBackgroundImage = -1;

	protected boolean gameOver;

	public Level( Player player, Resources resources ) {

		_entities = new AVLTree<Integer, Entity>();

		this.resources = resources;

		this.gravity = INITIAL_GRAVITY;
		this.friction = INITIAL_FRICTION;

		this.gameOver = false;

		this.player = player;
		try {
			loadLevel( 1 );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	public float getGravity() {
		return gravity;
	}

	public float getFriction() {
		return friction;
	}

	public int addEntity( Entity e ) throws Exception {
		int key = nextEntityKey;
		// _entities.add( key, new MapEntry<Integer, Entity>( key, e ) );
		_entities.Add( key, e );

		nextEntityKey++;

		return key;
	}

	public void removeEntity( int key ) {
		_entities.Remove( key );

	}

	public void clearEntities() throws Exception {

		_entities.Clear();
		nextEntityKey = 0;

		addEntity( player );

		addEntity( new MapPart( 0, 0, 5, Game.HEIGHT, false ) );
		addEntity( new MapPart( 0, 0, Game.WIDTH, 5, false ) );
		addEntity( new MapPart( 0, Game.HEIGHT - 5, Game.WIDTH, 5, false ) );
		addEntity( new MapPart( Game.WIDTH - 5, 0, 5, Game.HEIGHT, false ) );

	}

	public Entity getEntity( int index ) {
		return _entities.get( index ).getValue();
	}

	public boolean getGameOver() {
		return gameOver;
	}
	
	public void setGameOver( boolean gameOver){
		this.gameOver = gameOver;
	}

	public void render( GameContainer gc, Graphics g ) {

		for ( MapEntry<Integer, Entity> map : _entities ) {

			map.getValue().render( gc, g );

		}

	}

	public Snowball getSnowball() {
		return snowball;
	}

	public void setSnowball( Snowball snowball ) {
		this.snowball = snowball;
		try {
			snowballKey = addEntity( this.snowball );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
	}

	public Player getPlayer() {
		return player;
	}

	public void update( int delta ) {

		for ( MapEntry<Integer, Entity> map : _entities ) {
			if ( map.getValue() != player ) map.getValue().update( this );
		}

		// if ( snowball != null ) {
		// snowball.update( this );
		// }
	}

	public ArrayList<MapEntry<Integer, Entity>> CollidesWith( Entity entity ) {
		ArrayList<MapEntry<Integer, Entity>> returnList = new ArrayList<MapEntry<Integer, Entity>>();

		for ( MapEntry<Integer, Entity> map : _entities ) {
			if ( map.getValue().IntersectsWorld( entity ) ) {
				returnList.add( map );
			}
		}

		return returnList;
	}

	public void trigger( Entity entity, MapEntry<Integer, Entity> map ) {

		if ( map.getValue() instanceof Threshold ) {
			Threshold t = (Threshold) map.getValue();
			t.triggerActions( this );

		}

	}

	public void snowballHit( Snowball snowball, MapEntry<Integer, Entity> map ) {
		if ( map.getValue() == player ) {
			resources.Hit.play();
		}
		removeEntity( snowballKey );
		this.snowball = null;

		if ( map.getValue() == player && !hasPlayedFirstSnowballHit ) {
			playFirstSnowballHit();
		}
	}

	private void playFirstSnowballHit() {
		hasPlayedFirstSnowballHit = true;
		resources.Audio[3].play();
		try {
			loadLevel( 4 );
		} catch ( Exception e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void playerDied() {
		resources.Audio[7].play();
		while ( resources.Audio[7].playing() ) {
			try {
				Thread.sleep( 500 );
			} catch ( InterruptedException e ) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			loadLevel( 1 );
		} catch ( Exception e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		hasPlayedFirstSnowballHit = false;
		player.moveTo( STARTING_X, STARTING_Y );
	}

	public Image getCurrentBackground() {
		if ( currentBackgroundImage > -1 ) { return resources.BackgroundImages[currentBackgroundImage]; }
		return null;
	}

	public void loadLevel( int levelNumber ) throws Exception {
		clearEntities();

		Action[][] actions;

		switch ( levelNumber ) {
		case 1:
			actions = new Action[][] { { new Action_PlaySound( resources.Audio[0], false, true ) }, { new Action_PlaySound( resources.Audio[1], false, true ), new Action_LoadLevel( 2, true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level1.txt" ).getPath().toString(), actions );
			addEntity( new TextBox( 30f, 30f, 100f, 100f, new String[] { "Use the arrow keys, right and left, to run.  Up arrow key to jump.", "Walk to the left." }, Color.black ) );
			break;

		case 2:
			actions = new Action[][] { { new Action_LoadLevel( 3, true ), new Action_PlaySound( resources.Audio[2], false, true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level2.txt" ).getPath().toString(), actions );
			break;

		case 3:
			actions = new Action[][] { {} };
			loadFromFile( ResourceLoader.getResource( "res/level3.txt" ).getPath().toString(), actions );
			break;

		case 4:
			actions = new Action[][] { { new Action_LoadLevel( 5, true ), new Action_PlaySound( resources.Audio[4], false, true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level4.txt" ).getPath().toString(), actions );
			break;

		case 5:
			actions = new Action[][] { { new Action_LoadLevel( 6, true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level5.txt" ).getPath().toString(), actions );
			break;

		case 6:
			actions = new Action[][] { { new Action_LoadLevel( 7, true ), new Action_PlaySound( resources.Audio[5], false, true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level6.txt" ).getPath().toString(), actions );
			break;

		case 7:
			actions = new Action[][] { { new Action_LoadLevel( 8, true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level7.txt" ).getPath().toString(), actions );
			break;

		case 8:
			actions = new Action[][] { { new Action_LoadLevel( 9, true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level8.txt" ).getPath().toString(), actions );
			break;

		case 9:
			actions = new Action[][] { { new Action_PlaySound( resources.Audio[6], false, true ), new Action_SetGameState( true, true ) }, { new Action_PlaySound( resources.Audio[8], true, true ), new Action_PlaySound( resources.Audio[9], false, true ), new Action_AtHome( true ) } };
			loadFromFile( ResourceLoader.getResource( "res/level9.txt" ).getPath().toString(), actions );
			break;

		}
	}

	public void loadFromFile( String FileName, Action[][] actions ) throws SlickException, Exception {
		InputStream stream = ResourceLoader.getResourceAsStream( FileName );

		BufferedReader reader = new BufferedReader( new InputStreamReader( stream ) );

		String line;
		int y = 0;
		byte[] buffer;
		int id;

		Threshold t;
		line = reader.readLine();
		currentBackgroundImage = Integer.valueOf( line );

		while ( reader.ready() ) {
			line = reader.readLine();
			buffer = line.getBytes();

			for ( int x = 0; x < buffer.length; x++ ) {
				id = buffer[x] - 48;
				if ( buffer[x] == 'p' ) {
					addEntity( new MapPart( x * 25 + 5, y * 25 + 5, 25, 3, false ) );
				}
				if ( buffer[x] == 'f' ) {
					addEntity( new Fire( x * 25 + 5, y * 25 + 13 ) );
				}
				if ( buffer[x] == 'h' ) {
					addEntity( new House( x * 25, y * 25 - 30 ) );
				}
				if ( buffer[x] == 'b' ) {
					addEntity( new Package( x * 25, y * 25 - 2 ) );
				}
				if ( buffer[x] == 's' ) {
					addEntity( new Snowman( x * 25, y * 25 - 2, Snowman.MODE_INERT ) );
				}
				if ( buffer[x] == 'S' ) {
					addEntity( new Snowman( x * 25, y * 25 - 2, Snowman.MODE_HOSTILE ) );
				}
				if ( id > 0 && id < 10 ) {
					t = new Threshold( x * 25 + 5, y * 25 + 5, 25, 25, false );
					t.addActions( actions[id - 1] );
					addEntity( t );
				}
			}
			y++;
		}

		reader.close();
		stream.close();

	}
}
