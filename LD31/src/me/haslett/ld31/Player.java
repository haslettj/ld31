package me.haslett.ld31;

import java.util.ArrayList;
import me.haslett.collections.MapEntry;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

public class Player extends Entity {

	public static final String PLAYER_SPRITES_FILE_NAME_IDLE = "res/player_idle.png";
	public static final String PLAYER_SPRITES_FILE_NAME_JUMP = "res/player_jump.png";
	public static final String PLAYER_SPRITES_FILE_NAME_RUN_LEFT = "res/player_run_left.png";
	public static final String PLAYER_SPRITES_FILE_NAME_RUN_RIGHT = "res/player_run_right.png";
	public static final String PLAYER_SPRITES_FILE_NAME_FALL = "res/player_falling.png";
	public static final String PLAYER_SPRITES_FILE_NAME_PARACHUTE = "res/player_parachute.png";
	public static final String NAME = "Player";
	public static final int HIT_BOX_WIDTH = 12;
	public static final int HIT_BOX_X = 10;
	public static final int HIT_BOX_Y = 16;
	public static final int HIT_BOX_HEIGHT = 32;

	public static final float JUMP_FORCE = 6.0f;
	public static final float MOVE_ACCELERATION = 1.0f;
	public static final float MOVE_MAX_SPEED = 4.0f;

	public static final float VELOCITY_DEATH = 13f;
	public static final float VELOCITY_PARACHUTE_MAX = 8f;

	public static final int ANIMATION_COUNT = 6;
	public static final int ANIMATION_IDLE = 0;
	public static final int ANIMATION_RUN_RIGHT = 1;
	public static final int ANIMATION_RUN_LEFT = 2;
	public static final int ANIMATION_JUMP = 3;
	public static final int ANIMATION_FALL = 4;
	public static final int ANIMATION_PARACHUTE = 5;

	public static final int[] ANIMATION_FRAME_COUNTS = new int[] { 2, 2, 2, 2, 3, 1 };
	public static final int[] ANIMATION_DURATIONS = new int[] { 200, 200, 200, 100, 200, 200 };
	public static final boolean[] ANIMATION_PING_PONG = new boolean[] { false, false, false, false, true, false };
	public static final boolean[] ANIMATION_LOOPING = new boolean[] { true, true, true, false, true, false };

	public static final float WIDTH = 32f;
	public static final float HEIGHT = 48f;

	public Player( float x, float y ) throws SlickException {
		super( x, y, WIDTH, HEIGHT, true );

		showHitBox = false;

		_name = NAME;

		hitBoxColor = Color.pink;
		hitBox = new Rectangle( HIT_BOX_X, HIT_BOX_Y, HIT_BOX_WIDTH, HIT_BOX_HEIGHT );

		moveAccel = MOVE_ACCELERATION;
		moveMaxSpeed = MOVE_MAX_SPEED;
		jumpForce = JUMP_FORCE;

		animationSheets = new SpriteSheet[ANIMATION_COUNT];
		animationSheets[ANIMATION_IDLE] = new SpriteSheet( PLAYER_SPRITES_FILE_NAME_IDLE, (int) WIDTH, (int) HEIGHT );
		animationSheets[ANIMATION_RUN_RIGHT] = new SpriteSheet( PLAYER_SPRITES_FILE_NAME_RUN_RIGHT, (int) WIDTH, (int) HEIGHT );
		animationSheets[ANIMATION_RUN_LEFT] = new SpriteSheet( PLAYER_SPRITES_FILE_NAME_RUN_LEFT, (int) WIDTH, (int) HEIGHT );
		animationSheets[ANIMATION_JUMP] = new SpriteSheet( PLAYER_SPRITES_FILE_NAME_JUMP, (int) WIDTH, (int) HEIGHT );
		animationSheets[ANIMATION_FALL] = new SpriteSheet( PLAYER_SPRITES_FILE_NAME_FALL, (int) WIDTH, (int) HEIGHT );
		animationSheets[ANIMATION_PARACHUTE] = new SpriteSheet( PLAYER_SPRITES_FILE_NAME_PARACHUTE, (int) WIDTH, (int) HEIGHT );

		animations = new Animation[ANIMATION_COUNT];
		for ( int i = 0; i < ANIMATION_COUNT; i++ ) {
			animations[i] = new Animation( animationSheets[i], 0, 0, ANIMATION_FRAME_COUNTS[i] - 1, 0, true, ANIMATION_DURATIONS[i], true );
			// animations[i] = new Animation( animationSheet, 0, i,
			// ANIMATION_FRAME_COUNTS[i] - 1, i, true, ANIMATION_DURATIONS[i],
			// true );
			animations[i].setPingPong( ANIMATION_PING_PONG[i] );
			animations[i].setLooping( ANIMATION_LOOPING[i] );
		}

		currentAnimation = ANIMATION_IDLE;

	}
	
	
	public void reset(){
		isFalling = false;
		isJumping = false;
		velocityX = 0;
		velocityY = 0;
		hasParachute = false;
	}


	public void move( int direction, Level level ) {
		if ( canMove ) {
			float maxLocalMoveAcceleration = moveAccel < level.getFriction() ? moveAccel : level.getFriction();

			maxLocalMoveAcceleration *= 2.0f; // acceleration can be double
												// friction, no more!

			switch ( direction ) {
			case MOVE_DIRECTION_RIGHT:
				if ( !( isJumping || isFalling ) || ( Math.abs( velocityX ) < 0.01f ) ) {

					velocityX += isJumping && maxLocalMoveAcceleration < 0.1f ? 0.1f : maxLocalMoveAcceleration;
				}
				if ( velocityX > moveMaxSpeed ) velocityX = moveMaxSpeed;
				rightleftAnimationCounter = TICKS_TO_PLAY_RIGHT_LEFT_ANIMATION;
				break;

			case MOVE_DIRECTION_LEFT:
				if ( !( isJumping || isFalling ) || ( Math.abs( velocityX ) < 0.01f ) ) {
					velocityX -= isJumping && maxLocalMoveAcceleration < 0.1f ? 0.1f : maxLocalMoveAcceleration;
				}
				if ( velocityX < -moveMaxSpeed ) velocityX = -moveMaxSpeed;
				rightleftAnimationCounter = TICKS_TO_PLAY_RIGHT_LEFT_ANIMATION;
				break;

			case MOVE_DIRECTION_UP:
				if ( !( isJumping || isFalling ) ) {
					velocityY -= jumpForce;
					isJumping = true;
					if ( jumpSound != null ) {
						jumpSound.play();
					}
				}
				break;

			default:
				break;
			}
		}
	}

	@Override
	public void update( Level level ) {

		if ( canMove ) {
			velocityY += level.getGravity();
			if ( hasParachute ) {
				if ( velocityY > VELOCITY_PARACHUTE_MAX ) velocityY = VELOCITY_PARACHUTE_MAX;
			}

			isFalling = false;

			if ( velocityY >= 0 ) {
				isFalling = true;
				isJumping = false;
			}

			y += velocityY;

			// Process Falling and Jumping:

			if ( isFalling ) {
				ArrayList<MapEntry<Integer, Entity>> collisions = level.CollidesWith( this );

				float yOffset = hitBox.getMaxY();

				for ( MapEntry<Integer, Entity> map : collisions ) {
					if ( map.getValue() != this ) {
						float mapY = map.getValue().getWorldCollisionBox().getY();

						if ( ( y + yOffset > mapY ) && map.getValue().getBlocksOnCollision() ) {
							if ( velocityY > VELOCITY_DEATH ) {
								level.playerDied();

								break;
							}
							velocityY = 0;
							y = mapY - yOffset - 0.1f;
							isJumping = false;
							isFalling = false;

							lastBoxStoodOn = map.getKey();
						}

					}
				}

			} else if ( isJumping ) {

				ArrayList<MapEntry<Integer, Entity>> collisions = level.CollidesWith( this );

				for ( MapEntry<Integer, Entity> map : collisions ) {
					if ( map.getValue() != this ) {
						float mapMaxY = map.getValue().getWorldCollisionBox().getMaxY();

						if ( ( y + hitBox.getY() < mapMaxY ) && map.getValue().getBlocksOnCollision() ) {
							velocityY = 0;
							y = mapMaxY - hitBox.getY() + 0.1f;
							isJumping = false;
							isFalling = true;
						}

					}
				}
			}

			// Process move right or left

			if ( velocityX != 0 ) {
				float xOffset = hitBox.getMaxX();
				float yOffset = hitBox.getMaxY();

				int direction;

				x += velocityX;

				if ( velocityX > 0 ) {
					direction = MOVE_DIRECTION_RIGHT;
					if ( !( isJumping || isFalling ) ) {
						velocityX -= level.getFriction();
					}
					if ( velocityX < 0 ) {
						velocityX = 0;
						rightleftAnimationCounter = 0;
					}
				} else {
					direction = MOVE_DIRECTION_LEFT;
					if ( !( isJumping || isFalling ) ) {
						velocityX += level.getFriction();
					}
					if ( velocityX > 0 ) {
						velocityX = 0;
						rightleftAnimationCounter = 0;
					}
				}

				ArrayList<MapEntry<Integer, Entity>> collisions = level.CollidesWith( this );
				for ( MapEntry<Integer, Entity> map : collisions ) {
					if ( map.getValue() != this ) {
						float collisionMinX = map.getValue().getWorldCollisionBox().getX();
						float collisionMinY = map.getValue().getWorldCollisionBox().getY();
						float collisionMaxY = map.getValue().getWorldCollisionBox().getMaxY();

						if ( ( collisionMinY > y && collisionMinY < y + yOffset ) || ( collisionMaxY > y && collisionMaxY < y + yOffset ) || ( y > collisionMinY && y < collisionMaxY )
								|| ( y + yOffset > collisionMinY && y + yOffset < collisionMaxY ) ) {

							if ( map.getValue().getBlocksOnCollision() ) {
								if ( direction == MOVE_DIRECTION_RIGHT ) {
									x = collisionMinX - xOffset - 1;
									velocityX = 0;

								} else if ( direction == MOVE_DIRECTION_LEFT ) {
									x = map.getValue().getWorldCollisionBox().getMaxX() + 1 - hitBox.getX();
									velocityX = 0;

								}
							}
							if ( map.getValue().triggersOnCollision ) {
								level.trigger( this, map );
							}

						}
					}
				}

			}
		}

		if ( isJumping ) {
			currentAnimation = ANIMATION_JUMP;
		} else if ( isFalling ) {
			if ( hasParachute ) currentAnimation = ANIMATION_PARACHUTE;
			else currentAnimation = ANIMATION_FALL;
		} else if ( velocityX > 0 && rightleftAnimationCounter > 0 ) {
			currentAnimation = ANIMATION_RUN_RIGHT;
			rightleftAnimationCounter--;
		} else if ( velocityX < 0 && rightleftAnimationCounter > 0 ) {
			currentAnimation = ANIMATION_RUN_LEFT;
			rightleftAnimationCounter--;
		} else {
			currentAnimation = ANIMATION_IDLE;
		}
	}



}
