package me.haslett.ld31;

public class Action_MoveEntity extends Action {

	Entity entity;
	float newX, newY;
	
	public Action_MoveEntity(Entity entity, float newX, float newY, boolean fireOnce ) {
		super( fireOnce );

		this.newX = newX;
		this.newY = newY;
		this.entity = entity;
	}

	@Override
	public int go( Level level ) {

		entity.moveTo(newX, newY);
		
		return 0;
	}

}
