package me.haslett.ld31;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class House extends Entity {
	public static final String HOUSE_SPRITES_FILE = "res/house.png";
	public static final String NAME = "House";
	public static final float WIDTH = 64f;
	public static final float HEIGHT = 64f;

	public House( float x, float y ) throws SlickException{
		super( x, y, WIDTH, HEIGHT, false );
		
		_name = NAME;

		animationSheets = new SpriteSheet[1];
		animationSheets[0] = new SpriteSheet( HOUSE_SPRITES_FILE, (int) WIDTH, (int) HEIGHT );

		animations = new Animation[1];
		animations[0] = new Animation( animationSheets[0], 0, 0, 4, 0, true, 200, true );
		animations[0].setPingPong( false );
		animations[0].setLooping( true );
		
		currentAnimation = 0;
	}
}
