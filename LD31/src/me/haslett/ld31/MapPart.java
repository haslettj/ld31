package me.haslett.ld31;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class MapPart extends Entity {

	public MapPart( float x, float y, float width, float height, boolean canMove ) {
		super( x, y, width, height, canMove );

	}

	@Override
	public void render( GameContainer gc, Graphics g ) {

		Color origColor = g.getColor();

		g.setColor( Color.black );
		g.fillRect( x, y, hitBox.getWidth(), hitBox.getHeight() );

		g.setColor( origColor );
	}

}
