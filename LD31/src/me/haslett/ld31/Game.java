package me.haslett.ld31;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Game extends BasicGame {

	public static final int WIDTH = 960;
	public static final int HEIGHT = 540;
	public static final String TITLE = "Little Dude's Epic Adventure";
	
	public static final String[] END_CREDITS = new String[]{"You got Little Dude home!  You Win!",  "", "", "This game was made in under 48 hours for Ludum Dare #31.", "", "",
		"If you enjoyed playing it, please rate appropriately.", "", "", "Thank you for Playing!"};

	protected Boolean gameStatePaused = false;

	protected Level level;
	protected Player player;
	protected Resources resources;

	public Game() {
		super( TITLE );

	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

	@Override
	public void render( GameContainer gc, Graphics g ) throws SlickException {

		if ( level.getGameOver() ) {
			
			g.drawImage( resources.BackgroundImages[0], 0, 0 );
			g.setColor( Color.black );
			
			for ( int i = 0; i < END_CREDITS.length; i++ ) {
				g.drawString( END_CREDITS[i], 10, 10 + ( i * g.getFont().getLineHeight() ) );
			}

		} else {
			Image background = level.getCurrentBackground();
			if ( background != null ) {
				g.drawImage( background, 0, 0 );
			} else {
				g.setColor( Color.white );
				g.fillRect( 0, 0, gc.getWidth(), gc.getHeight() );

			}

			level.render( gc, g );
			// player.render( gc, g );
		}
	}

	@Override
	public void init( GameContainer gc ) throws SlickException {
		gc.setMinimumLogicUpdateInterval( 16 );
		gc.setMaximumLogicUpdateInterval( 16 );
		gc.setShowFPS( false );

		resources = new Resources();

		player = new Player( Level.STARTING_X, Level.STARTING_Y );

		level = new Level( player, resources );

		try {
			level.addEntity( player );
		} catch ( Exception e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		resources.music.loop( 1f, 0.05f );

		// resources.Audio_1.play();
	}

	@Override
	public void update( GameContainer gc, int delta ) throws SlickException {
		if ( !level.getGameOver() ) {
			Input input = gc.getInput();

			if ( input.isKeyPressed( Input.KEY_W ) || input.isKeyPressed( Input.KEY_UP ) ) player.move( Player.MOVE_DIRECTION_UP, level );
			if ( input.isKeyDown( Input.KEY_D ) || input.isKeyDown( Input.KEY_RIGHT ) ) player.move( Player.MOVE_DIRECTION_RIGHT, level );
			if ( input.isKeyDown( Input.KEY_A ) || input.isKeyDown( Input.KEY_LEFT ) ) player.move( Player.MOVE_DIRECTION_LEFT, level );
			if ( input.isKeyPressed( Input.KEY_SPACE ) ) gameStatePaused = true;

			player.update( level );
			level.update( delta );

		}
	}

	public static void main( String[] args ) throws SlickException {
		AppGameContainer app = new AppGameContainer( new Game(), (int) Game.WIDTH, (int) Game.HEIGHT, false );

		app.start();
	}

}
