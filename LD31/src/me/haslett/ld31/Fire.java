package me.haslett.ld31;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Fire extends Entity {
	public static final String FIRE_SPRITE_FILE_NAME = "res/fire.png";
	public static final String NAME = "Fire";
	public static final float WIDTH = 16f;
	public static final float HEIGHT = 16f;

	public Fire( float x, float y ) throws SlickException {
		super( x, y, WIDTH, HEIGHT, false );

		_name = NAME;

		animationSheets = new SpriteSheet[1];
		animationSheets[0] = new SpriteSheet( FIRE_SPRITE_FILE_NAME, (int) WIDTH, (int) HEIGHT );

		animations = new Animation[1];
		animations[0] = new Animation( animationSheets[0], 0, 0, 5, 0, true, 200, true );
		animations[0].setPingPong( false );
		animations[0].setLooping( true );
		
		currentAnimation = 0;

	}

}
