package me.haslett.ld31;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class TextBox extends Entity {

	protected String[] text;
	protected Color color;

	public TextBox( float x, float y, float width, float height, String[] text, Color color ) {
		super( x, y, width, height, false );

		this.text = text;
		this.color = color;
	}

	@Override
	public void render( GameContainer gc, Graphics g ) {

		Color origColor = g.getColor();

		g.setColor( color );

		for ( int i = 0; i < text.length; i++ ) {
			g.drawString( text[i], x, y + ( i * g.getFont().getLineHeight() ) );
		}

		if ( showHitBox ) {
			g.setColor( hitBoxColor );
			g.drawRect( x + hitBox.getX(), y + hitBox.getY(), hitBox.getWidth(), hitBox.getHeight() );
		}

		g.setColor( origColor );

	}
}
