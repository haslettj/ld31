package me.haslett.ld31;

import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class Resources {



	public static final int AUDIO_NUMBER_CLIPS =10;
	
	public static final int BACKGROUND_IMAGE_FILES = 1;
	
	public static final String SOUND_HIT_FILENAME = "res/hit.wav";
	public static final String MUSIC_FILENAME = "res/music.ogg";
	
	public Sound[] Audio;

	public Sound Hit;
	
	public Image[] BackgroundImages;
	
	public Music music;
	
	public Resources() {
		
		try {
	
			Audio = new Sound[AUDIO_NUMBER_CLIPS];
			BackgroundImages = new Image[BACKGROUND_IMAGE_FILES];
			
			for( int i = 0; i < AUDIO_NUMBER_CLIPS; i++){
				Audio[i] = new Sound( "res/audio" + Integer.toString( i ) + ".ogg" );
			}
		
			for( int i = 0; i < BACKGROUND_IMAGE_FILES; i++){
				BackgroundImages[i] = new Image( "res/background" + Integer.toString( i ) + ".png" );
			}
			
			Hit = new Sound( SOUND_HIT_FILENAME );
			
			music = new Music( MUSIC_FILENAME );

		} catch ( SlickException e ) {
			e.printStackTrace();
		}
	}


}
