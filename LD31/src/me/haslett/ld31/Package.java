package me.haslett.ld31;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Package extends Entity{
	public static final String PACKAGE_SPRITE_FILE_NAME = "res/package.png";
	public static final String NAME = "Package";
	public static final float WIDTH = 32f;
	public static final float HEIGHT = 32f;

	public Package( float x, float y ) throws SlickException {
		super( x, y, WIDTH, HEIGHT, false );

		_name = NAME;

		animationSheets = new SpriteSheet[1];
		animationSheets[0] = new SpriteSheet( PACKAGE_SPRITE_FILE_NAME, (int) WIDTH, (int) HEIGHT );

		animations = new Animation[1];
		animations[0] = new Animation( animationSheets[0], 0, 0, 0, 0, true, 200, true );
		animations[0].setPingPong( false );
		animations[0].setLooping( true );
		
		currentAnimation = 0;

	}

}
