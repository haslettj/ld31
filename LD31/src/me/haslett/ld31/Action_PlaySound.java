package me.haslett.ld31;

import org.newdawn.slick.Sound;

public class Action_PlaySound extends Action {

	protected Sound sound;
	protected boolean blocks;

	public Action_PlaySound( Sound sound, boolean blocks, boolean fireOnce ) {
		super( fireOnce );
		this.sound = sound;
		this.blocks = blocks;
	}

	@Override
	public int go( Level level ) {
		if ( !fireOnce || fireCount < 1 ) {
			fireCount++;
			sound.play();
			if ( blocks ) {
				while ( sound.playing() ) {
					try {
						Thread.sleep( 500 );
					} catch ( InterruptedException e ) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		return 0;
	}
}
