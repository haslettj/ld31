package me.haslett.ld31;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;

public abstract class Entity {

	public static final int TICKS_TO_PLAY_RIGHT_LEFT_ANIMATION = 10;

	public static final int MOVE_DIRECTION_UP = 1;
	public static final int MOVE_DIRECTION_DOWN = 2;
	public static final int MOVE_DIRECTION_LEFT = 4;
	public static final int MOVE_DIRECTION_RIGHT = 8;

	public static final float MOVE_SPEED = 3f;

	protected float x, y;
	protected float velocityX = 0f;
	protected float velocityY = 0f;

	protected float moveAccel = 0.0f;
	protected float moveMaxSpeed = 0.0f;
	protected float jumpForce = 0.0f;

	protected boolean isJumping;
	protected boolean isFalling;
	protected boolean hasParachute = false;

	protected boolean visible = true;

	protected boolean canMove;

	protected int lastBoxStoodOn = -1;

	protected int rightleftAnimationCounter = 0;

	protected SpriteSheet[] animationSheets;
	protected Animation[] animations;

	protected int currentAnimation;

	protected Sound jumpSound;

	protected String _name = "Unknown Entity";
	protected Rectangle hitBox;
	protected boolean showHitBox = false;

	protected boolean blocksOnCollision = true;

	protected boolean triggersOnCollision = false;

	protected Color hitBoxColor = Color.white;

	public Entity( float x, float y, float width, float height, boolean canMove ) {

		this( x, y, width, height, canMove, true, false );
	}

	public Entity( float x, float y, float width, float height, boolean canMove, boolean blocks, boolean triggers ) {
		this.x = x;
		this.y = y;

		this.canMove = canMove;

		hitBox = new Rectangle( 0, 0, width, height );

		blocksOnCollision = blocks;
		triggersOnCollision = triggers;

	}

	public float getWidth() {
		return hitBox.getWidth();
	}

	public float getHeight() {
		return hitBox.getHeight();
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getVelocityY() {
		return velocityY;
	}

	public float getVelocityX() {
		return velocityX;
	}

	public boolean getBlocksOnCollision() {
		return blocksOnCollision;
	}
	
	public void setBlocksOnCollision( boolean b ) {
		blocksOnCollision = b;
	}

	public boolean getTriggersOnCollision() {
		return triggersOnCollision;
	}

	public void setTriggersOnCollision( boolean triggersOnCollision ) {
		this.triggersOnCollision = triggersOnCollision;
	}

	public void triggerActions( Level level ) {}
	
	public void setParachute( boolean b ) {
		hasParachute = b;
	}

	public void setJumpSound( Sound sound ) {
		jumpSound = sound;
	}

	public Rectangle getWorldCollisionBox() {
		return new Rectangle( x + hitBox.getX(), y + hitBox.getY(), hitBox.getWidth(), hitBox.getHeight() );
	}

	public String getName() {
		return _name;
	}

	public void setName( String name ) {
		_name = name;
	}

	public void render( GameContainer gc, Graphics g ) {
		if ( visible ) {
			if ( animations != null ) {
				animations[currentAnimation].draw( x, y );
			}

			if ( showHitBox ) {
				Color origColor = g.getColor();
				g.setColor( hitBoxColor );
				g.drawRect( x + hitBox.getX(), y + hitBox.getY(), hitBox.getWidth(), hitBox.getHeight() );
				g.setColor( origColor );
			}
		}

	}

	public void update( Level level ) {

	}

	public void moveTo( float newX, float newY ) {
		x = newX;
		y = newY;
		velocityX = 0;
		velocityY = 0;
		isFalling = false;
		isJumping = false;

	}

	public boolean IntersectsWorld( Entity entity ) {
		return getWorldCollisionBox().intersects( entity.getWorldCollisionBox() );
	}

	public String getDebug() {
		return "(" + x + ", " + y + ")";
	}

}
