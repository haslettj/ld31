package me.haslett.ld31;

public class Action_Clear extends Action {

	public Action_Clear( boolean fireOnce ) {
		super( fireOnce );

	}

	@Override
	public int go( Level level ) {
		if ( !fireOnce || fireCount < 1 ) {
			fireCount++;
			try {
				level.clearEntities();
			} catch ( Exception e ) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return 0;
	}

}
