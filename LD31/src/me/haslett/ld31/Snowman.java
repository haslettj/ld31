package me.haslett.ld31;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Snowman extends Entity {

	public static final String SNOWMAN_SPRITE_FILE_NAME = "res/snowman.png";
	public static final String NAME = "Snowman";
	public static final float WIDTH = 26f;
	public static final float HEIGHT = 32f;

	public static final int MODE_INERT = 0;
	public static final int MODE_PEACEFUL = 1;
	public static final int MODE_HOSTILE = 2;

	public static final int SNOWBALL_DISTANCE = 150;

	protected int mode;

	public Snowman( float x, float y, int mode ) throws SlickException {
		super( x, y, WIDTH, HEIGHT, false );

		_name = NAME;

		animationSheets = new SpriteSheet[1];
		animationSheets[0] = new SpriteSheet( SNOWMAN_SPRITE_FILE_NAME, (int) WIDTH, (int) HEIGHT );

		animations = new Animation[1];
		animations[0] = new Animation( animationSheets[0], 0, 0, 0, 0, true, 200, true );

		currentAnimation = 0;
		blocksOnCollision = false;
		showHitBox = false;

		setMode( mode );
	}

	public int getMode() {
		return mode;
	}

	public void setMode( int mode ) {
		this.mode = mode;

		switch ( this.mode ) {
		case MODE_INERT:
			blocksOnCollision = false;
			break;
		case MODE_PEACEFUL:
			blocksOnCollision = true;
			break;
		case MODE_HOSTILE:
			blocksOnCollision = true;
			break;

		}
	}

	@Override
	public void update( Level level ) {
		super.update( level );

		if ( level.getSnowball() == null && mode == MODE_HOSTILE ) {
			if ( level.getPlayer().getX() > x - SNOWBALL_DISTANCE && level.getPlayer().getX() < x + getWidth() + SNOWBALL_DISTANCE && level.getPlayer().getY() > y - 32
					&& level.getPlayer().getY() < y + getHeight() ) {
				try {
					if ( level.getPlayer().getX() < x ) {
						level.setSnowball( new Snowball( x - 5, y ) );
						level.getSnowball().setVelocityX( -5f );
					} else {
						level.setSnowball( new Snowball( x + getWidth() + 5, y ) );
						level.getSnowball().setVelocityX( 5f );
					}

				} catch ( Exception e ) {
					e.printStackTrace();
				}
			}

		}
	}

	public void setVisible( boolean visible ) {
		this.visible = visible;
	}
}
